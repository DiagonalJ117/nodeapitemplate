module.exports = app => {
    const people = require("../controllers/person.controller.js");
    var router = require("express").Router();
    const multer = require('multer');
    const upload = multer();

    //create new person
    router.post("/", upload.any(), people.create);

    //retrieve all people
    router.get("/" , people.findAll);

    //Retrieve all people with website
    router.get("/withweb", people.findAllWithWebsite);

    //retrieve single person with id
    router.get("/:id", people.findOne);

    //Update a person with id
    router.put("/:id", upload.any(), people.update);

    //Delete a person with id
    router.delete("/:id", people.delete);
    
    //Delete all people
    router.delete("/", people.deleteAll);

    app.use('/api/people', router);
};