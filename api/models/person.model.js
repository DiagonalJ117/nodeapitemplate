module.exports = (sequelize, Sequelize) => {
    const Person = sequelize.define("person", {
      name: {
        type: Sequelize.STRING
      },
      birthdate: {
        type: Sequelize.DATE
      },
      profilepic: {
        type: Sequelize.STRING
      },
      website:{
          type: Sequelize.STRING
      }
    });
  
    return Person;
  };