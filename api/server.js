const dotenv = require('dotenv');
/**
 * Use this if your .env file is outside the /api/ folder, in the root folder.
 * dotenv.config({path: '../.env'});
 */
dotenv.config();
const express = require("express");
const cors = require("cors");

const app = express();


const db = require("./models");

const Op = db.Sequelize.Op;


/**
 * Use this to reset the db when you reload the API. ONLY in development mode
 */

if(process.env.SEQUELIZE_RESET == "true" && process.env.NODE_ENV == "development"){
  db.sequelize.sync({ force: true }).then(() => {
       console.log("Drop and re-sync db.");
       initial();
    });
}else{
  db.sequelize.sync();
}

var corsOptions = {
  origin: process.env.CORS_ORIGIN || "http://localhost"
};

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: false}));


// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to Superintendent application. SEQRESET: "+process.env.SEQUELIZE_RESET +" DB HOST: "+ process.env.DB_HOST+ " DB PORT: "+process.env.DB_PORT});
});

/**
 * Routes.
 */
require("./routes/person.routes.js")(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});