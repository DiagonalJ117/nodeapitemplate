module.exports = {
  /**
   * sinodedb is the name of the container with the MySQL instance
   */

  HOST: process.env.DB_HOST,
  /**
   * HOST: "localhost",
   * Use localhost as HOST if you're running it in your locally OUTSIDE a container
   */
  
  USER: process.env.DB_USER,
  PASSWORD: process.env.DB_PASSWORD,
  DB: process.env.DB_NAME,
  /**
   * PORT: 3308
   * Run in port 3308 if you run it with HOST:localhost
   */
  PORT: process.env.DB_PORT,
  dialect: "mysql",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};