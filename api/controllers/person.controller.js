const db = require('../models')
const Person = db.persons;
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
    if (!req.body.name) {
        res.status(400).send({
          message: "Name can not be empty!"
        });
        return;
      }
    
    const person = {
        name: req.body.name,
        birthdate: req.body.birthdate,
        profilepic: req.body.profilepic,
        website: req.body.website
    }

    Person.create(person).then((data) => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Person."
        })
    })
};

// Retrieve all Person from the database.
exports.findAll = (req, res) => {
    const name = req.query.name;
    var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;

  Person.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving people."
      });
    });
};

// Find a single Person with an id
exports.findOne = (req, res) => {
  const id = req.params.id
  Person.findByPk(id).then(data => {
      res.send(data)
  }).catch(err=> {
      res.status(500).send({
          message: "Error retrieving person with id=" + id
      })
  })
};

// Update a Person by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  Person.update(req.body, {
      where: { id: id}
  }).then(num => {
    if(num != 1){
        res.send({
            message: `Cannot update Person with id=${id}. Maybe Person was not found or req.body is empty`
        })
    }
  }).then(() => {
    Person.findByPk(id).then(rec => {
        res.send({
            data: rec.dataValues
        })
    }).catch(err => {
        console.log(err.toString())
    })
  }).catch(err => {
      res.status(500).send({
          message: "Error updating Person with id="+id
      })
  })
};

// Delete a Person with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Person.destroy({
      where: {id: id}
  }).then(num => {
      if(num == 1){
        res.send({
            message: `Person with id ${id} was deleted successfully`
        });
      }else{
          res.send({
              message: `Cannot delete Person with id ${id}`
          })
      }
  }).catch(err => {
    res.status(500).send({
        message: "Could not delete Person with id=" + id + ". Reason: "+ err.toString()
      });
  });
};

// Delete all Persons from the database.
exports.deleteAll = (req, res) => {
  Person.destroy({
      where: {},
      truncate: false
  }).then(nums => {
      res.send({ message: `${nums} People were deleted successfully` })
  }).catch(err => {
      res.status(500).send({
          message: err.message || "Some error ocurred while removing all people."
      })
  })
};

/* find objects by condition  */

/* in this case we check if the website string is not null or empty */
exports.findAllWithWebsite = (req, res) => {
    Person.findAll({ where: { website:{ [Op.or]: { [Op.not]:"", [Op.or]:null }  }  }}).then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error ocurred while retrieving people."
        })
    })
}
