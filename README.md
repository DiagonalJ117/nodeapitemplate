# Node API Example

I made this as a template for my projects where I need a Node REST API with a MySQL database. (because I find more projects where I need to use MySQL)

I might also make a Mongo version for later. Or you can fork it and make one too :D

## Setup
Install all dependencies
```
npm install
```
## Setup docker mysql container

```
docker-compose -f docker-compose-db.yml up
```

## Setup docker api container and mysql container
```
docker-compose -f docker-compose-api-db.yml up
```

## OR connect to your own db
by modifying the file in /api/config/db.config.js


## Start the API
```
node server.js
```

## Start the API with nodemon to reload it when the files are changed
```
nodemon ./server.js
```

I also included a Postman collection if you want to test the API in Postman. Just import the collection into Postman and you'll be set for testing.

Special thanks to Bezkoder. I used [**this tutorial**](https://bezkoder.com/node-js-express-sequelize-mysql/) to make this.

**Happy coding!**